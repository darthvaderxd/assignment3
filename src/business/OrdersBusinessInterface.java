package business;

import java.util.List;
import javax.ejb.Local;
import beans.MyOrder;

@Local
public interface OrdersBusinessInterface {
	public List<MyOrder> getOrders();
	public void sendOrder(MyOrder order);
}
