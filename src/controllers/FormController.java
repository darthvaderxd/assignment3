package controllers;

import java.io.Serializable;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.ejb.EJB;
import javax.faces.bean.ManagedBean;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import beans.MyOrder;
import beans.User;
import business.MyTimerService;
import business.OrdersBusinessInterface;

@ManagedBean(name = "formController")
@ViewScoped
public class FormController implements  Serializable {
	private static final long serialVersionUID = 1L;
	private User user;
	
	@Inject
	private OrdersBusinessInterface orders;
	
	@EJB
	private MyTimerService timer;
	
	private Connection conn = null;
	
	@PostConstruct
	public void init() {
		getOrders();
	}
	
	public User getUser() {
		try {
			FacesContext context = FacesContext.getCurrentInstance();
			User savedUser = (User) context.getExternalContext().getRequestMap().get("user");
			if (savedUser != null) {
				user = savedUser;
			}
		} catch (Exception e) {
			// do nothing
		}
		return user;
	}

	public void setUser(User user) {
		this.user = user;
	}
	
	public String onFlash(User user) {
		FacesContext context = FacesContext.getCurrentInstance();
        context.getExternalContext().getFlash().put("user", user);
		return "TestResponse2.xhtml?faces-redirect=true";
	}
	
	private void getAllOrders() {
		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "abc@123");
			Statement stmt = conn.createStatement();
			String sql = "SELECT * FROM testapp.Orders";
			ResultSet rs = stmt.executeQuery(sql);
			
			while(rs.next()) {
				System.out.println("price => " + rs.getDouble("price"));
			}
			rs.close();
			
			System.out.println("Success!!");
		} catch (SQLException e) {
			System.out.println("Failure!!");
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public String onLogoff() {
		// Invalidate the Session to clear the security token
		FacesContext.getCurrentInstance().getExternalContext().invalidateSession();
			
		// Redirect to a protected page (so we get a full HTTP Request) to get Login Page
		return "TestResponse.xhtml?faces-redirect=true";
	}
	
	private void insertOrder() {
		try {
			conn = DriverManager.getConnection("jdbc:postgresql://localhost:5432/postgres", "postgres", "abc@123");
			Statement stmt = conn.createStatement();
			String sql = "INSERT INTO  testapp.ORDERS(ORDER_NO, PRODUCT_NAME, PRICE, QUANTITY) VALUES('001122334455', 'This was inserted new', 25.00, 100)";
			stmt.executeUpdate(sql);
		} catch (SQLException e) {
			System.out.println("Failure!!");
		} finally {
			if (conn != null) {
				try {
					conn.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		}
	}
	
	public List<MyOrder> getOrders() {
		return orders.getOrders();
	}
	
	public String goToStore() {
		return "TestResponse.xhtml";
	}
	
	public String onSendOrder() {
		orders.sendOrder(orders.getOrders().get(0));
		return "OrderResponse.xhtml";
	}
}
