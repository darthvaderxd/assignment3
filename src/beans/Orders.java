package beans;

import java.util.ArrayList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;

import business.OrdersBusinessService;

@ManagedBean(name = "orders")
@ViewScoped
public class Orders {
	@Inject
	private OrdersBusinessService service;
	
	List<MyOrder> orders = new ArrayList<MyOrder>();
	
	public Orders() {}
	
	@PostConstruct
	public void init() {
		orders = service.getOrders();
	}

	public List<MyOrder> getOrders() {
		return orders;
	}

	public void setOrders(List<MyOrder> orders) {
		this.orders = orders;
	}
}
