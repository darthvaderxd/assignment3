package data;

import java.util.List;
import beans.MyOrder;

public interface DataAccessInterface {
	public List<MyOrder> findAll();
	public MyOrder findById(int id);
	public void create(MyOrder order);
	public void update(MyOrder order);
	public void delete(MyOrder order);
}
