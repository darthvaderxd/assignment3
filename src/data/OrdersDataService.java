package data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

import beans.MyOrder;

/**
 * Session Bean implementation class OrdersDataService
 */
@Stateless
@LocalBean
public class OrdersDataService implements DataAccessInterface {
	@PersistenceContext(unitName="assignment4c")
	private EntityManager em;

    /**
     * Default constructor. 
     */
    public OrdersDataService() {
        // TODO Auto-generated constructor stub
    }

	@Override
	public List<MyOrder> findAll() {
		return em.createNamedQuery("MyOrder.findAll", MyOrder.class ).getResultList();
	}

	@Override
	public MyOrder findById(int id) {
		return em.find(MyOrder.class, id);
	}

	@Override
	public void create(MyOrder order) {
		em.persist(order);
	}

	@Override
	public void update(MyOrder order) {
		em.merge(order);
	}

	@Override
	public void delete(MyOrder order) {
		em.remove(order);
	}

}
